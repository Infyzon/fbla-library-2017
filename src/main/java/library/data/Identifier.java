package library.data;

import java.util.Objects;

public class Identifier implements Comparable<Identifier> {
    private final String id;

    public Identifier(String id) {
        this.id = padLeftWithZeros(id);
    }

    public Identifier(int id) {
        this.id = String.format("%06d", id);
    }

    private static String padLeftWithZeros(String str) {
        StringBuilder padded = new StringBuilder(str);
        while (padded.length() < 6) {
            padded.insert(0, '0');
        }
        return padded.toString();
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        return this == o || (o instanceof Identifier && Objects.equals(id, ((Identifier) o).id));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return id;
    }

    @Override
    public int compareTo(Identifier o) {
        return id.compareTo(o.id);
    }
}
