package library.data;

public interface LibraryData {
    Identifier getIdentifier();
    String[] asData();
}
