package library;

import javafx.application.Application;
import library.fx.FXInitializer;

/**
 * The starting point of the application
 * @author Srikavin Ramkumar
 */
public class Main {
    public static void main(String[] args){
        //Start the JavaFX GUI
        Application.launch(FXInitializer.class);
    }
}
