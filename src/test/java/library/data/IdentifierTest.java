package library.data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IdentifierTest {
    @Test
    void identifierTest() {
        Identifier identifier = new Identifier("123");
        assertEquals("000123", identifier.getId());

        Identifier identifier1 = new Identifier(123);
        assertEquals("000123", identifier1.getId());
        assertEquals("000123", identifier1.getId());
    }
}